import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthLayoutRoutes } from './auth-layout.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from '../../pages/home/home.component';
import { InfoComponent } from 'src/app/pages/info/info.component';
import { CarouselModule } from 'primeng/carousel';
import { SidebarContentComponent } from 'src/app/pages/sidebar-content/sidebar-content.component';
import { ProjectComponent  } from '../../pages/project/project.component';
import { ContactComponent } from '../../pages/contact/contact.component';
import { SudicoComponent } from '../../pages/project/sudico/sudico.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthLayoutRoutes),
    FormsModule,
    // NgbModule,
    CarouselModule,
    
  ],
  declarations: [
    HomeComponent,
    InfoComponent,
    SidebarContentComponent,
    ProjectComponent,
    ContactComponent,
    SudicoComponent
  ]
})
export class AuthLayoutModule { }
