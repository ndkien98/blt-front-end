import { Routes } from '@angular/router';

import { RegisterComponent } from '../../pages/register/register.component';
import { HomeComponent } from '../../pages/home/home.component'
import { InfoComponent } from 'src/app/pages/info/info.component';
import { ProjectComponent  } from '../../pages/project/project.component';
import { SudicoComponent } from 'src/app/pages/project/sudico/sudico.component';
import { ContactComponent } from 'src/app/pages/contact/contact.component';

export const AuthLayoutRoutes: Routes = [
    { path: 'home',          component: HomeComponent },
    { path: 'info',    component: InfoComponent},
    { path: 'register',       component: RegisterComponent },
    { path : 'geleximco',     component : ProjectComponent},
    { path : 'sudico',        component : SudicoComponent},
    { path : 'contact',       component: ContactComponent}
];
