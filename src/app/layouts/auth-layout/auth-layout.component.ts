import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { MenuItem } from 'primeng/api';
import * as uuid from 'uuid';
@Component({
    selector: 'app-auth-layout',
    templateUrl: './auth-layout.component.html',
    styleUrls: ['./auth-layout.component.scss'],

})
export class AuthLayoutComponent implements OnInit, OnDestroy {
    test: Date = new Date();
    public isCollapsed = true;
    items: MenuItem[];
    bigMenu: any[] = [];
    item: any;
    selectedMenu: any;
    
    isProjects: boolean;
    isLandSerice: boolean;
    islandscape: boolean;
    sidebar_content: boolean = false;


    listDropDownMenu: any;
    isDisabel = false;
    menus: any[] = [];
    constructor(private router: Router) {
        this.isProjects = false;
        this.isLandSerice = false;
        this.islandscape = false;
    }
    onSelect($event): void {
        localStorage.setItem('current_menu_id', $event.target.id);
        this.selectedMenu = $event;
        this.item = $event.target;
        console.log(this.selectedMenu, '=========================>')
    }
    viewItemMenu = (id) => {
        if (id == 'project') {
            this.isProjects = true;
        } else {
            this.isProjects = false;
        }
        console.log('item_id', id);
    }
    @HostListener('document:click', ['$event'])
    hoverBtn ($event: any):void {
        if ($event.target.matches('#project') ) {
            this.isProjects = true;
            console.log('true')
        } else {
            this.isProjects = false;
        }

        if ($event.target.matches('#service')) {
            this.isLandSerice = true;
        } else {
            this.isLandSerice = false;
        }
        
        if ($event.target.matches('#landscape')) {
            this.islandscape = true;
        } else {
            this.islandscape = false;
        }

    }
    ngOnInit() {
        var html = document.getElementsByTagName("html")[0];
        html.classList.add("auth-layout");
        var body = document.getElementsByTagName("body")[0];
        this.router.events.subscribe((event) => {
            this.isCollapsed = true;
        });
         this.menus = [
            {
                "label": "Trang chủ",
                "data": "",
                "collapsedIcon": "pi pi-home",
                "link":"/home"
            },
            {
                "label": "Giới thiệu",
                "data": "P",
                "collapsedIcon": "pi pi-info-circle",
                "link":"/info"
            },
            {
                "label": "Dự án",
                "data": "",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "children": [{
                        "label": "Dự án 1",
                        "data": "Pacino Movies",
                        "link":"/info"
                    },
                    {
                        "label": "Dự án 2",
                        "data": "De Niro Movies",
                        "link":"/info"
                    }]
            },
            {
                "label": "Đất dịch vụ",
                "data": "",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "children": [{
                        "label": "An thượng",
                        "data": "Pacino Movies",
                        "link":"/info"
                    },
                    {
                        "label": "An khánh",
                        "data": "De Niro Movies",
                        "link":"/info"
                    }]
            },
            {
                "label": "Đất thổ cư",
                "data": "",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "children": [{
                        "label": "Dự án 1",
                        "data": "Pacino Movies",
                        "link":"/info"
                    },
                    {
                        "label": "Dự án 2",
                        "data": "De Niro Movies",
                        "link":"/info"
                    }]
            }

         ] 
    }

    ngOnDestroy() {
        var html = document.getElementsByTagName("html")[0];
        html.classList.remove("auth-layout");
        var body = document.getElementsByTagName("body")[0];
        body.classList.remove("bg-default");
    }
    prepareRoute(outlet: RouterOutlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation']
    }
    onNodeSelect ($event: any) {
        if ($event && $event.node && $event.node.link) {
            this.router.navigateByUrl($event.node.link);
        }
    }
}
