import { SpinnerService } from './progress-spinner.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress-spinner.component.html',
  styleUrls: ['./progress-spinner.component.css']
})
export class ProgressSpinnerComponent implements OnInit {
  isShow: boolean;
  constructor(
    public spinnerService: SpinnerService
  ) {
  }

  ngOnInit(): void {
    this.spinnerService.returnAsObservable().subscribe(
      showStatus => {
        this.isShow = showStatus;
      });
  }

  show = () => {
    this.isShow = true;
  }

  hide = () => {
    this.isShow = false;
  }
}
