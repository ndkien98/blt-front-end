import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { BrowserModule } from '@angular/platform-browser';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';  

import { TreeModule } from 'primeng/tree';


// import { JwtInterceptor } from '../app/comon/jwt.interceptor';
// import { ErrorInterceptor } from '../app/comon/error.interceptor';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import { SidebarModule } from 'primeng/sidebar';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ProgressSpinnerComponent} from './commont/progress-spinner/progress-spinner.component';
import { SlideMenuModule } from 'primeng/slidemenu';
import { LogginComponent } from './pages/loggin/loggin.component';
import { RegisterComponent } from './pages/register/register.component';
import { ToastModule } from 'primeng/toast';
import { ConfirmationService, MessageService } from 'primeng/api';
import { SpinnerService } from './commont/progress-spinner/progress-spinner.service';


// import { fakeBackendProvider } from './comon/fake-backend';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    AccordionModule,
    SidebarModule,
    ReactiveFormsModule,
    SlideMenuModule,
    CommonModule,
    BrowserModule,
    TreeModule,
    ProgressSpinnerModule,
    ToastModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent, 
    ProgressSpinnerComponent,
    LogginComponent,
    RegisterComponent
    
  ],
  providers : [MessageService, ConfirmationService, SpinnerService],
  // providers: [
  //   { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  //   { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

  //   // provider used to create fake backend
  //   fakeBackendProvider],  
    
    bootstrap: [AppComponent]
})
export class AppModule { }
