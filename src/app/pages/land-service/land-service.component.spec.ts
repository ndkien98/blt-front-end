import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandServiceComponent } from './land-service.component';

describe('LandServiceComponent', () => {
  let component: LandServiceComponent;
  let fixture: ComponentFixture<LandServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
