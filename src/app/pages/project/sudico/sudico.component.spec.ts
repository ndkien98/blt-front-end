import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SudicoComponent } from './sudico.component';

describe('SudicoComponent', () => {
  let component: SudicoComponent;
  let fixture: ComponentFixture<SudicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SudicoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SudicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
