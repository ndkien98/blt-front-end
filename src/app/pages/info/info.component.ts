import { Component, OnInit } from '@angular/core';
import { SpinnerService } from 'src/app/commont/progress-spinner/progress-spinner.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  constructor( private spinner: SpinnerService) { }

  ngOnInit(): void {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 1500);
  }

}
